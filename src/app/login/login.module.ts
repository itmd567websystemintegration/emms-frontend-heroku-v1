import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AuthService } from './auth/auth.service';
import { CallbackComponent } from './callback/callback.component';
import { StudentService } from './../layout/services/student.service';
import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { HomeComponent } from './home/home.component';
@NgModule({
    imports: [
        CommonModule,
        LoginRoutingModule
    ],
    declarations: [LoginComponent, HomeComponent, CallbackComponent],
    providers: [AuthService, StudentService]
})
export class LoginModule {
}