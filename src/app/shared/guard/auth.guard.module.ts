import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { StudentService } from './../../layout/services/student.service';
@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [],
    providers: [StudentService]
})
export class AuthGuardModule {
}