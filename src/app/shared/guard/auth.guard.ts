import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import { StudentService } from './../../layout/services/student.service';
import { AuthService } from './../../login/auth/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
    // private emailSent: boolean = false;
    // private usernameExists: boolean;
    // private emailExists: boolean;
    // private username: string;
    // private email: string;
    // private givenName: string;
    // private familyName: string;
    // private idToken: string;
    // private  profile: any;
    
    constructor(private router: Router, private studentService: StudentService, public auth: AuthService) { }

    canActivate() {
        console.log('canActivate');
        console.log(this.auth.isAuthenticated());
        if (this.auth.isAuthenticated()) {
           // this.auth.setProfile();
            return true;
        }
        this.router.navigate(['/login']);
        return false;
    }

   
    // setProfile() {
    //       this.idToken =   localStorage.getItem('id_token');
    //       this.auth.getProfile((err, profile) => {
    //         this.profile = profile;
    //         console.log(JSON.stringify(this.profile.nickname));
    //         this.onNewAccount(this.profile, this.idToken);
    //       });
    //   }

    // onNewAccount(profile: any, idToken: string) {
    //     console.log(JSON.stringify(this.profile));
    //     this.emailSent = false;
    //     if(this.profile != null) {
    //         this.username = this.profile.nickname;
    //         this.email = this.username+"@gmail.com";
    //         this.givenName = this.profile.given_name;
    //         this.familyName = this.profile.family_name;
    //         this.idToken = idToken;
    //     }
        
        
    //     this.studentService.addStudent(this.username, this.email, this.givenName, this.familyName, this.idToken).subscribe(
    //         res => {
    //             console.log(res);
    //             this.emailSent = true;
    //         },
    //         error => {
    //             console.log(error.text());
    //             let errorMessage = error.text();
    //             if (errorMessage === "usernameExists") this.usernameExists = true;
    //             if (errorMessage === "emailExists") this.emailExists = true;
    //         }
    //     );
    // }

    // public isAuthenticated(): boolean {
    //     // Check whether the current time is past the
    //     // access token's expiry time
    //     const expiresAt = JSON.parse(localStorage.getItem('expires_at'));
    //     return new Date().getTime() < expiresAt;
    // }
}
