import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '', component: LayoutComponent,
        children: [
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'charts', loadChildren: './charts/charts.module#ChartsModule' },
            { path: 'tables', loadChildren: './tables/tables.module#TablesModule' },
            { path: 'forms', loadChildren: './form/form.module#FormModule' },
            { path: 'bs-element', loadChildren: './bs-element/bs-element.module#BsElementModule' },
            { path: 'grid', loadChildren: './grid/grid.module#GridModule' },
            { path: 'components', loadChildren: './bs-component/bs-component.module#BsComponentModule' },
            { path: 'blank-page', loadChildren: './blank-page/blank-page.module#BlankPageModule' },
            { path: 'view-book/:id', loadChildren: './components/view-book/view-book.module#ViewBookModule' },
            { path: 'view-notes/:id', loadChildren: './components/view-notes/view-notes.module#ViewNotesModule' },
            { path: 'view-lab/:id', loadChildren: './components/view-lab/view-lab.module#ViewLabModule' },
            { path: 'view-order/:id', loadChildren: './components/view-order/view-order.module#ViewOrderModule' },
            { path: 'edit-book/:id', loadChildren: './components/edit-book/edit-book.module#EditBookModule' },
            { path: 'edit-notes/:id', loadChildren: './components/edit-notes/edit-notes.module#EditNotesModule' },
            { path: 'edit-lab/:id', loadChildren: './components/edit-lab/edit-lab.module#EditLabModule' },
            { path: 'book-list', loadChildren: './components/book-list/book-list.module#BookListModule' },
            { path: 'student-cart', loadChildren: './components/shop/shop-cart/student-cart.module#StudentCartModule' },
            { path: 'place-order', loadChildren: './components/shop/place-order/place-order.module#OrderModule' },
            { path: 'order-history', loadChildren: './components/order-history/order-history.module#OrderHistoryModule' },
            { path: 'purchase-summary', loadChildren: './components/shop/purchase-summary/purchase-summary.module#PurchaseSummaryModule' },
            { path: 'shop-book-list', loadChildren: './components/shop/shop-book/shop-book-list.module#ShopBookListModule' },
            { path: 'shop-notes-list', loadChildren: './components/shop/shop-notes/shop-notes-list.module#ShopNotesListModule' },
            { path: 'shop-lab-list', loadChildren: './components/shop/shop-lab/shop-lab-list.module#ShopLabListModule' },
            { path: 'add-book', loadChildren: './components/add-new-book/add-new-book.module#AddNewBookModule' },
            { path: 'add-course', loadChildren: './components/add-new-course/add-new-course.module#AddNewCourseModule' },
            { path: 'add-department', loadChildren: './components/add-new-department/add-new-department.module#AddNewDepartmentModule' },
            { path: 'update-profile', loadChildren: './components/update-profile/update-profile.module#UpdateProfileModule' },
            { path: 'add-notes', loadChildren: './components/add-new-notes/add-new-notes.module#AddNewNotesModule' },
            { path: 'add-lab', loadChildren: './components/add-new-lab/add-new-lab.module#AddNewLabModule' },
            { path: 'notes-list', loadChildren: './components/notes-list/notes-list.module#NotesListModule' },
            { path: 'lab-list', loadChildren: './components/lab-list/lab-list.module#LabListModule' },

            //purchase
            { path: 'book-detail/:id', loadChildren: './components/book-detail/book-detail.module#BookDetailModule' },
            { path: 'notes-detail/:id', loadChildren: './components/notes-detail/notes-detail.module#NotesDetailModule' },
            { path: 'lab-detail/:id', loadChildren: './components/lab-detail/lab-detail.module#LabDetailModule' },
             
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule { }
