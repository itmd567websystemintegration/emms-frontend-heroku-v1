import {Department} from './department';
import {Course} from './course';
import {Student} from './student';

export class Notes {
	public id: number;
	public title: string;
	public professor: string;
	public notesdetails1: string;
	public notesdetails2: string;
	public notesdetails3: string;
	public price: number;
    public description: string;
    public student: Student;
    public department: Department;
	public course: Course;
}
