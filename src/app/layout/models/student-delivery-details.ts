export class StudentDeliveryDetails {
	public id: number;
	public studentDeliveryName: string;
	public studentDeliveryStreet1: string;
	public studentDeliveryStreet2: string;
	public studentDeliveryCity: string;
	public studentDeliveryState: string;
	public studentDeliveryCountry: string;
	public studentDeliveryPostalcode: string;
	public studentDeliveryDefault: boolean;
}
