import {StudentPaymentDetails} from './student-payment-details';
import {StudentDeliveryDetails} from './student-delivery-details';

export class Student {
	public id: number;
	public firstName: string;
	public lastName: string;
	public userName: string;
	public password: string;
	public emailId: string
	public phone: string;
	public isEnabled: boolean;
	public accessToken: string;
	public studentPaymentDetailsList: StudentPaymentDetails[];
	public studentDeliveryDetailsList: StudentDeliveryDetails[];
	
}
