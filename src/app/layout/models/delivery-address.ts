import {StudentOrder} from './student-order';

export class DeliveryAddress {
	public id: number;
	public deliveryAddressName: string;
	public deliveryAddressStreet1: string;
	public deliveryAddressStreet2: string;
	public deliveryAddressCity: string;
	public deliveryAddressState: string;
	public deliveryAddressCountry: string;
	public deliveryAddressPostalcode: string;
	public order: StudentOrder;
}
