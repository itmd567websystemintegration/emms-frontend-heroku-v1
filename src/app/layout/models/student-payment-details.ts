import {StudentBillingDetails} from './student-billing-details';

export class StudentPaymentDetails {
	public id: number;
	public cardType: string;
	public cardName: string;
    public cardNumber: string;
    public cardHolderName: string;
    public cvc: number;
	public expiryMonth: string;
	public expiryYear: string;
    public studentBillingDetails: StudentBillingDetails;
    public defaultPayment: boolean;
}
