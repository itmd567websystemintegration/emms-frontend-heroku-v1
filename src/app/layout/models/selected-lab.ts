import {LabMaterial} from './lab';
import {StudentCart} from './student-cart';

export class SelectedLabMaterial {
	public id: number;
	public lab: LabMaterial;
	public shoppingCart: StudentCart
	public toUpdate:boolean;
}
