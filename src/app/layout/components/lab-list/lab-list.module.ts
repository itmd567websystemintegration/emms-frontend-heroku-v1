import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LabListRoutingModule } from './lab-list.routing.module';
import { LabListComponent } from './lab-list.component';
import { PageHeaderModule } from './../../../shared';
import { DataFilterPipe } from './../../components/lab-list/data-filter.pipe';
import { DataTableModule } from 'angular2-datatable';


import { GetLabListService } from './../../services/get-lab-list.service';
import {DeleteLabService} from '../../services/delete-lab.service';
import { AddItemService } from './../../services/add-item.service';
import { UploadImageService } from './../../services/upload-image.service';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        LabListRoutingModule,
        PageHeaderModule,
        DataTableModule
    ],
declarations: [LabListComponent, DataFilterPipe],
providers: [
    AddItemService,
    UploadImageService,
    GetLabListService,
    DeleteLabService
  ]
})
export class LabListModule { }
