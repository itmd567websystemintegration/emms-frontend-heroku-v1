import { Component, OnInit } from '@angular/core';
import { Notes } from '../../models/notes';
import { ItemService } from '../../services/item.service';
import { StudentCartService } from '../../services/student-cart.service';
import {Params, ActivatedRoute, Router} from '@angular/router';
import {Http} from '@angular/http';
import {AppProperties} from '../../constants/app-properties';

@Component({
  selector: 'app-notes-detail',
  templateUrl: './notes-detail.component.html',
	styleUrls: ['./notes-detail.component.scss']
})
export class NotesDetailComponent implements OnInit {
	private notesId: number;
	private notes: Notes = new Notes();
	private serverPath = AppProperties.serverPath;
	private addNotesSuccess: boolean = false;

  constructor(
  	private itemService:ItemService,
    private studentCartService: StudentCartService,
		private router:Router,
		private http:Http,
		private route:ActivatedRoute
  	) { }

  onAddNotesToStudentCart() {
    this.studentCartService.addNotes(this.notesId).subscribe(
      res => {
        console.log(res.text());
        this.addNotesSuccess=true;
      },
      err => {
        console.log(err.text());
      }
    );
  }



  ngOnInit() {

  	this.route.params.forEach((params: Params) => {
  		this.notesId = Number.parseInt(params['id']);
  	});

  	this.itemService.getNotes(this.notesId).subscribe(
  		res => {
				this.notes=res.json();
				console.log(JSON.stringify(this.notes));
  		},
  		error => {
  			console.log(error);
  		}
  	);
  }
}
