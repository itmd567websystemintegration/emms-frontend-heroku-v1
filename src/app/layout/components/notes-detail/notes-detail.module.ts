import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItemService } from '../../services/item.service';
import { StudentCartService } from '../../services/student-cart.service';
import { NotesDetailRoutingModule } from './notes-detail.routing.module';
import { NotesDetailComponent } from './notes-detail.component';
import { PageHeaderModule } from './../../../shared';

@NgModule({
    imports: [
        CommonModule,
        NotesDetailRoutingModule,
        PageHeaderModule
    ],
declarations: [NotesDetailComponent],
providers: [StudentCartService, ItemService]
})
export class NotesDetailModule { }
