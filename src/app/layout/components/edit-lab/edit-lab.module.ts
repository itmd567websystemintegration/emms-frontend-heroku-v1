import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { EditLabRoutingModule } from './edit-lab.routing.module';
import { EditLabComponent } from './edit-lab.component';
import { PageHeaderModule } from './../../../shared';

import { GetCourseListService } from './../../services/get-course-list.service';
import { GetDepartmentListService } from './../../services/get-department-list.service';
import { GetLabService } from './../../services/get-lab.service';
import { EditLabService } from './../../services/edit-lab.service';
import { UploadImageService } from './../../services/upload-image.service';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        EditLabRoutingModule,
        PageHeaderModule
    ],
declarations: [EditLabComponent],
providers: [
    GetDepartmentListService,
    GetCourseListService,
    GetLabService,
    EditLabService,
    UploadImageService
  ]
})
export class EditLabModule { }
