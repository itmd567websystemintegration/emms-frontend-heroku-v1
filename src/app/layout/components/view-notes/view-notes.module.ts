import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ViewNotesRoutingModule } from './view-notes.routing.module';
import { ViewNotesComponent } from './view-notes.component';
import { PageHeaderModule } from './../../../shared';

import { GetNotesService } from './../../services/get-notes.service';
import { UploadImageService } from './../../services/upload-image.service';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ViewNotesRoutingModule,
        PageHeaderModule
    ],
declarations: [ViewNotesComponent],
providers: [
    GetNotesService,
    UploadImageService
  ]
})
export class ViewNotesModule { }
