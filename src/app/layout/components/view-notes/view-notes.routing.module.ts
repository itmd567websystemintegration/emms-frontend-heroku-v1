import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewNotesComponent } from './view-notes.component';

const routes: Routes = [
    { path: '', component: ViewNotesComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewNotesRoutingModule { }
