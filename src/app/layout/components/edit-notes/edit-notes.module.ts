import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { EditNotesRoutingModule } from './edit-notes.routing.module';
import { EditNotesComponent } from './edit-notes.component';
import { PageHeaderModule } from './../../../shared';

import { GetCourseListService } from './../../services/get-course-list.service';
import { GetDepartmentListService } from './../../services/get-department-list.service';
import { GetNotesService } from './../../services/get-notes.service';
import { EditNotesService } from './../../services/edit-notes.service';
import { UploadImageService } from './../../services/upload-image.service';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        EditNotesRoutingModule,
        PageHeaderModule
    ],
declarations: [EditNotesComponent],
providers: [
    GetDepartmentListService,
    GetCourseListService,
    GetNotesService,
    EditNotesService,
    UploadImageService
  ]
})
export class EditNotesModule { }
