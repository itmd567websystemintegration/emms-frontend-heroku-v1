import { Component, OnInit } from '@angular/core';
// import { MatGridListModule } from '@angular/material';
import { UploadImageService } from '../../services/upload-image.service';
import { Notes } from '../../models/notes';
import { Course } from '../../models/course';
import { Department } from '../../models/department';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { GetNotesService } from '../../services/get-notes.service';
import { GetDepartmentListService } from '../../services/get-department-list.service';
import { EditNotesService } from '../../services/edit-notes.service';
import { GetCourseListService } from '../../services/get-course-list.service';

@Component({
	selector: 'app-edit-notes',
	templateUrl: './edit-notes.component.html',
	styleUrls: ['./edit-notes.component.scss']
})
export class EditNotesComponent implements OnInit {
	private notesId: number;
	private notes: Notes = new Notes();
	private department: Department = new Department();
	private course: Course = new Course();
	private notesUpdated: boolean;
	private departmentList: Department[] = [];
	private courseList: Course[] = [];

	constructor(
		private uploadImageService: UploadImageService,
		private editNotesService: EditNotesService,
		private getNotesService: GetNotesService,
		private getCourseListService: GetCourseListService,
		private getDepartmentListService: GetDepartmentListService,
		private route: ActivatedRoute,
		private router: Router
	) { }

	onSubmit() {
		this.editNotesService.editNotes(this.notes).subscribe(

			data => {
				console.log("edited notes: " + this.notes);
				this.uploadImageService.modify(JSON.parse(JSON.parse(JSON.stringify(data))._body).id);
				this.notesUpdated = true;
				this.notes = new Notes();
			},
			error => console.log(error)
		);
	}

	ngOnInit() {
		this.route.params.forEach((params: Params) => {
			this.notesId = Number.parseInt(params['id']);
		});

		this.getNotesService.getNotes(this.notesId).subscribe(
			res => {
				this.notes = res.json();
				console.log("Notes to edit: "+JSON.stringify(this.notes));
				this.department = this.notes.department;
				this.course = this.notes.course;
			},
			error => console.log(error)
		)

		this.getDepartmentListService.getDepartmentList().subscribe(
			res => {
				console.log(res.json());
				this.departmentList = res.json();
			},
			error => {
				console.log(error.text());
			}
		);

		this.getCourseListService.getCourseList().subscribe(
			res => {
				console.log(res.json());
				this.courseList = res.json();
			},
			error => {
				console.log(error.text());
			}
		);
	}

}
