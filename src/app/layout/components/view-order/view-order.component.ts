import { Component, OnInit } from '@angular/core';
import {Params, ActivatedRoute, Router} from '@angular/router';
import {StudentOrderService} from '../../services/order.service';
import {StudentOrder} from '../../models/student-order';
import {Student} from '../../models/student';

@Component({
  selector: 'app-view-order',
  templateUrl: './view-order.component.html',
  styleUrls: ['./view-order.component.scss']
})
export class ViewOrderComponent implements OnInit {

  private studentOrder:StudentOrder = new StudentOrder();
  private studentOrderId: number;

  constructor(private studentOrderService:StudentOrderService,
  	private route:ActivatedRoute, private router:Router) { }


  ngOnInit() {
  	this.route.params.forEach((params: Params) => {
  		this.studentOrderId = Number.parseInt(params['id']);
  	});

  	this.studentOrderService.getStudentOrder(this.studentOrderId).subscribe(
  		res => {
  			this.studentOrder = res.json();
  		},
  		error => {
  			console.log(error);
  		}
  	);

  	
  }

}
