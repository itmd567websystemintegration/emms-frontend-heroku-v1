import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewLabComponent } from './view-lab.component';

const routes: Routes = [
    { path: '', component: ViewLabComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewLabRoutingModule { }
