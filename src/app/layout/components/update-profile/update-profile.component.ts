import { Component, OnInit } from '@angular/core';
import { AddItemService } from '../../services/add-item.service';
import { UploadImageService } from '../../services/upload-image.service';
import { AppProperties } from '../../constants/app-properties';
import { StudentService } from '../../services/student.service';
import { Student } from '../../models/student';
import { Router } from '@angular/router';
@Component({
	selector: 'app-update-profile',
	templateUrl: './update-profile.component.html',
	styleUrls: ['./update-profile.component.scss']
})
export class UpdateProfileComponent implements OnInit {
	private serverPath = AppProperties.serverPath;
	private dataFetched = false;
	private loginError:boolean;
	private loggedIn:boolean;
	private credential = {'username':'', 'password':''};

	private student: Student = new Student();
	private updateSuccess: boolean;
	private newPassword: string;
	private incorrectPassword: boolean;
	private currentPassword: string;
	private idToken: string;
	

	constructor(private studentService: StudentService, private router: Router) {
		this.idToken =   localStorage.getItem('id_token');
	 }

	
	onUpdateStudentDetails () {
		console.log("student to update: "+JSON.stringify(this.student));
		this.studentService.updateStudentDetails(this.student, this.newPassword, this.currentPassword).subscribe(
			data => {
				console.log(data.text());
				this.updateSuccess=true;
			},
			error => {
				console.log(error.text());
				let errorMessage = error.text();
				if(errorMessage==="Incorrect current password!") this.incorrectPassword=true;
			}
		);
	}

	getCurrentStudent() {
		this.studentService.getCurrentStudent(this.idToken).subscribe(
			res => {				
				this.student = res.json();
				this.dataFetched = true;
				console.log('current student from db: '+JSON.stringify(this.student));
			},
			err => {
				console.log(err);
			}
		);
	}
  
	ngOnInit() {
		this.getCurrentStudent();
	}
}
