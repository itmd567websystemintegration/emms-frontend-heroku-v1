import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { UpdateProfileRoutingModule } from './update-profile.routing.module';
import { PageHeaderModule } from './../../../shared';
import { StudentService } from './../../services/student.service';
import { UpdateProfileComponent } from './update-profile.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        UpdateProfileRoutingModule,
        PageHeaderModule
    ],
declarations: [UpdateProfileComponent],
providers: [
    StudentService
  ]
})
export class UpdateProfileModule { }
