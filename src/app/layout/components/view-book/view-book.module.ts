import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ViewBookRoutingModule } from './view-book.routing.module';
import { ViewBookComponent } from './view-book.component';
import { PageHeaderModule } from './../../../shared';

import { GetBookService } from './../../services/get-book.service';
import { UploadImageService } from './../../services/upload-image.service';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ViewBookRoutingModule,
        PageHeaderModule
    ],
declarations: [ViewBookComponent],
providers: [
    GetBookService,
    UploadImageService
  ]
})
export class ViewBookModule { }
