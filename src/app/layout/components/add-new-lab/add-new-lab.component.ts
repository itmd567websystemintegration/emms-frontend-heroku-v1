import { Component, OnInit } from '@angular/core';
import { LabMaterial } from '../../models/lab';
import { Department } from '../../models/department';
import { Course } from '../../models/course';
import { AddItemService } from '../../services/add-item.service';
import { UploadImageService } from '../../services/upload-image.service';
import { GetDepartmentListService } from '../../services/get-department-list.service';
import { GetCourseListService } from '../../services/get-course-list.service';
import { StudentService } from '../../../layout/services/student.service';
import { Student } from '../../../layout/models/student';

@Component({
	selector: 'app-add-new-lab',
	templateUrl: './add-new-lab.component.html',
	styleUrls: ['./add-new-lab.component.scss']
})
export class AddNewLabComponent implements OnInit {
	private newLab: LabMaterial = new LabMaterial();
	private course: Course = new Course();
	private department: Department = new Department();
	private labAdded: boolean;
	private departmentList: Department[] = [];
	private courseList: Course[] = [];
	private idToken: string;
	private student: Student = new Student();
	constructor(private studentService: StudentService, private addItemService: AddItemService, private uploadImageService: UploadImageService,
		private getDepartmentListService: GetDepartmentListService, private getCourseListService: GetCourseListService) { }

	onSubmit() {
		this.newLab.department = this.department;
		this.newLab.course = this.course;
		this.newLab.student = this.student;
		console.log(this.newLab);
		this.addItemService.addLab(this.newLab).subscribe(
			res => {
				this.uploadImageService.upload(JSON.parse(JSON.parse(JSON.stringify(res))._body).id, "lab");
				this.labAdded = true;
				this.newLab = new LabMaterial();
			},
			error => {
				console.log(error);
			}
		);
	}

	ngOnInit() {
		this.labAdded = false;

		this.idToken = localStorage.getItem('id_token');

		this.getDepartmentListService.getDepartmentList().subscribe(
			res => {
				console.log(res.json());
				this.departmentList = res.json();
			},
			error => {
				console.log(error.text());
			}
		);

		this.getCourseListService.getCourseList().subscribe(
			res => {
				console.log(res.json());
				this.courseList = res.json();
			},
			error => {
				console.log(error.text());
			}
		);

		this.studentService.getCurrentStudent(this.idToken).subscribe(
			res => {
				this.student = res.json();
				console.log('current student from db: ' + JSON.stringify(this.student));
			},
			err => {
				console.log(err);
			}
		);
	}
}
