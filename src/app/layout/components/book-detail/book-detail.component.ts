import { Component, OnInit } from '@angular/core';
import { Book } from '../../models/book';
import { ItemService } from '../../services/item.service';
import { StudentCartService } from '../../services/student-cart.service';
import {Params, ActivatedRoute, Router} from '@angular/router';
import {Http} from '@angular/http';
import {AppProperties} from '../../constants/app-properties';

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
	styleUrls: ['./book-detail.component.scss']
})
export class BookDetailComponent implements OnInit {

	private bookId: number;
	private book: Book = new Book();
	private serverPath = AppProperties.serverPath;
	private addBookSuccess: boolean = false;

  constructor(
  	private itemService:ItemService,
    private studentCartService: StudentCartService,
		private router:Router,
		private http:Http,
		private route:ActivatedRoute
  	) { }

  onAddBookToStudentCart() {
    this.studentCartService.addBook(this.bookId).subscribe(
      res => {
        console.log(res.text());
        this.addBookSuccess=true;
      },
      err => {
        console.log(err.text());
      }
    );
  }



  ngOnInit() {
  	this.route.params.forEach((params: Params) => {
  		this.bookId = Number.parseInt(params['id']);
  	});

  	this.itemService.getItem(this.bookId).subscribe(
  		res => {
  			this.book=res.json();
  		},
  		error => {
  			console.log(error);
  		}
  	);
  }

}
