import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItemService } from '../../services/item.service';
import { StudentCartService } from '../../services/student-cart.service';
import { BookDetailRoutingModule } from './book-detail.routing.module';
import { BookDetailComponent } from './book-detail.component';
import { PageHeaderModule } from './../../../shared';

@NgModule({
    imports: [
        CommonModule,
        BookDetailRoutingModule,
        PageHeaderModule
    ],
declarations: [BookDetailComponent],
providers: [StudentCartService, ItemService]
})
export class BookDetailModule { }
