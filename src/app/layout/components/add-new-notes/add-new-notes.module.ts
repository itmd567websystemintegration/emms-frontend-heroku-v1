import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AddNewNotesRoutingModule } from './add-new-notes.routing.module';
import { AddNewNotesComponent } from './add-new-notes.component';
import { PageHeaderModule } from './../../../shared';
import { StudentService } from '../../../layout/services/student.service';

import { GetDepartmentListService } from './../../services/get-department-list.service';
import { GetCourseListService } from './../../services/get-course-list.service';


import { AddItemService } from './../../services/add-item.service';
import { UploadImageService } from './../../services/upload-image.service';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        AddNewNotesRoutingModule,
        PageHeaderModule
    ],
declarations: [AddNewNotesComponent],
providers: [
    AddItemService,
    UploadImageService,
    GetDepartmentListService,
    GetCourseListService,
    StudentService
  ]
})
export class AddNewNotesModule { }
