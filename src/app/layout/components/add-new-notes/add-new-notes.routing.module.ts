import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddNewNotesComponent } from './add-new-notes.component';

const routes: Routes = [
    { path: '', component: AddNewNotesComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddNewNotesRoutingModule { }
