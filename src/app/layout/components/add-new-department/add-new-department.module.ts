import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AddNewDepartmentRoutingModule } from './add-new-department.routing.module';
import { AddNewDepartmentComponent } from './add-new-department.component';
import { PageHeaderModule } from './../../../shared';
import { GetDepartmentListService } from './../../services/get-department-list.service';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        AddNewDepartmentRoutingModule,
        PageHeaderModule
    ],
declarations: [AddNewDepartmentComponent],
providers: [
    GetDepartmentListService,
  ]
})
export class AddNewDepartmentModule { }
