import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PurchaseSummaryComponent } from './purchase-summary.component';

const routes: Routes = [
    { path: '', component: PurchaseSummaryComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PurchaseSummaryRoutingModule { }
