import { Component, OnInit } from '@angular/core';
import { Book } from '../../../models/book';
import {GetBookListService} from '../../../services/get-book-list.service';
import {Params, ActivatedRoute, Router} from '@angular/router';
import {Http} from '@angular/http';
import {AppProperties} from '../../../constants/app-properties';

@Component({
	selector: 'app-shop-book-list',
	templateUrl: './shop-book-list.component.html',
	styleUrls: ['./shop-book-list.component.scss']
})
export class ShopBookListComponent implements OnInit {
	public filterQuery = "";
	public rowsOnPage = 5;
	private selectedBook: Book;
	private bookList: Book[];
	private serverPath = AppProperties.serverPath;

	constructor(
		private getBookListService: GetBookListService,
		private router:Router,
		private http:Http,
		private route:ActivatedRoute
		) { }

	onSelect(book: Book) {
		this.selectedBook = book;
		this.router.navigate(['/book-detail', this.selectedBook.id]);
	}

	ngOnInit() {
		this.route.queryParams.subscribe(params => {
			if(params['bookList']) {
				console.log("shopping book list");
				this.bookList = JSON.parse(params['bookList']);
			} else {
				this.getBookListService.getBookList().subscribe(
					res => {
						console.log(res.json());
						this.bookList = res.json();
					},
					err => {
						console.log(err);
					}
					);
			}
		});

	}
}


