import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ShopBookListComponent } from './shop-book-list.component';

const routes: Routes = [
    { path: '', component: ShopBookListComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShopBookListRoutingModule { }
