import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { OrderRoutingModule } from './place-order.routing.module';
import { OrderComponent } from './place-order.component';
import { PageHeaderModule } from './../../../../shared';
import { StudentCartService } from './../../../services/student-cart.service';
import {StudentDeliveryDetailsService} from '../../../services/delivery.service';
import {PaymentService} from '../../../services/payment.service';
import {StudentCheckoutService} from '../../../services/checkout.service';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        OrderRoutingModule,
        PageHeaderModule,
    ],
declarations: [OrderComponent ],
providers: [
    StudentCartService, StudentCheckoutService, StudentDeliveryDetailsService, PaymentService ]
})
export class OrderModule { }
