import {Component, OnInit} from '@angular/core';
import {AppProperties} from '../../../constants/app-properties';
import {Book} from '../../../models/book';
import {Notes} from '../../../models/notes';
import {LabMaterial} from '../../../models/lab';
import {Router, NavigationExtras} from "@angular/router";
import {StudentCartService} from '../../../services/student-cart.service';
import {StudentDeliveryDetailsService} from '../../../services/delivery.service';
import {PaymentService} from '../../../services/payment.service';
import {StudentCheckoutService} from '../../../services/checkout.service';
import {SelectedBook} from '../../../models/selected-book';
import {SelectedNotes} from '../../../models/selected-notes';
import {SelectedLabMaterial} from '../../../models/selected-lab';
import {StudentCart} from '../../../models/student-cart';
import {DeliveryAddress} from '../../../models/delivery-address';
import {BillingAddress} from '../../../models/billing-address';
import {StudentPaymentDetails} from '../../../models/student-payment-details';
import {StudentBillingDetails} from '../../../models/student-billing-details';
import {StudentDeliveryDetails} from '../../../models/student-delivery-details';
import {Payment} from '../../../models/payment';
import {StudentOrder} from '../../../models/student-order';

@Component({
  selector: 'app-order',
  templateUrl: './place-order.component.html',
  styleUrls: ['./place-order.component.scss']
})
export class OrderComponent implements OnInit {
  private serverPath = AppProperties.serverPath;
  private selectedBook: Book;
  private selectedBookList: SelectedBook[] = [];
  private selectedNotes: Notes;
  private selectedNotesList: SelectedNotes[] = [];
  private selectedLabMaterial: LabMaterial;
  private selectedLabMaterialList: SelectedLabMaterial[] = [];
  private studentCart: StudentCart = new StudentCart();
  private deliveryAddress:DeliveryAddress=new DeliveryAddress();
  private billingAddress:BillingAddress = new BillingAddress();
  private studentPayment:StudentPaymentDetails = new StudentPaymentDetails();
  private studentDeliveryDetails:StudentDeliveryDetails = new StudentDeliveryDetails();
  private studentBilling: StudentBillingDetails = new StudentBillingDetails();
  private studentDeliveryList: StudentDeliveryDetails[] = [];
  private studentPaymentList: StudentPaymentDetails[] = [];
  private payment: Payment = new Payment();
  private selectedTab: number;
  private selectedBookNumber: number;
  private emptyShippingList: boolean = true;
  private emptyPaymentList: boolean = true;
  private stateList: string[] = [];
  private shippingMethod:string;
  private order:StudentOrder = new StudentOrder();

  constructor(
  	private router:Router, 
  	private studentCartService: StudentCartService, 
  	private deliveryService: StudentDeliveryDetailsService,
  	private paymentService: PaymentService,
  	private studentCheckoutService: StudentCheckoutService
  	) { }

  onSelect(book:Book) {
    this.selectedBook = book;
    this.router.navigate(['/book-detail', this.selectedBook.id]);
  }

  getSelectedBookList(){
    this.studentCartService.getSelectedBookList().subscribe(
      res=>{
        this.selectedBookList = res.json();
        this.selectedBookNumber = this.selectedBookList.length;
      },
      error=>{
        console.log(error.text());
      }
      );
  }

  getSelectedNotesList() {
		this.studentCartService.getSelectedNotesList().subscribe(
			res => {
				this.selectedNotesList = res.json();
				console.log(JSON.stringify(this.selectedNotesList));
				this.selectedBookNumber = this.selectedBookNumber + this.selectedNotesList.length;
			},
			error => {
				console.log(error.text());
			}
		)
	}

  getSelectedLabList() {
		this.studentCartService.getSelectedLabList().subscribe(
			res => {
				this.selectedLabMaterialList = res.json();
				console.log(JSON.stringify(this.selectedLabMaterialList));
				this.selectedBookNumber = this.selectedBookNumber + this.selectedNotesList.length;
			},
			error => {
				console.log(error.text());
			}
		)
  }
  
  setDeliveryAddress(studentDeliveryDetails: StudentDeliveryDetails) {
  	this.deliveryAddress.deliveryAddressName = studentDeliveryDetails.studentDeliveryName;
  	this.deliveryAddress.deliveryAddressStreet1 = studentDeliveryDetails.studentDeliveryStreet1;
  	this.deliveryAddress.deliveryAddressStreet2 = studentDeliveryDetails.studentDeliveryStreet2;
  	this.deliveryAddress.deliveryAddressCity = studentDeliveryDetails.studentDeliveryCity;
  	this.deliveryAddress.deliveryAddressState = studentDeliveryDetails.studentDeliveryState;
  	this.deliveryAddress.deliveryAddressCountry = studentDeliveryDetails.studentDeliveryCountry;
  	this.deliveryAddress.deliveryAddressPostalcode = studentDeliveryDetails.studentDeliveryPostalcode;
  }

  setPaymentMethod(studentPaymentDetails: StudentPaymentDetails) {
  	this.payment.cardType = studentPaymentDetails.cardType;
  	this.payment.cardNumber = studentPaymentDetails.cardNumber;
  	this.payment.expiryMonth = studentPaymentDetails.expiryMonth;
  	this.payment.expiryYear = studentPaymentDetails.expiryYear;
  	this.payment.cvc = studentPaymentDetails.cvc;
  	this.payment.cardHolderName = studentPaymentDetails.cardHolderName;
  	this.billingAddress.billingAddressName = studentPaymentDetails.studentBillingDetails.studentBillingName;
  	this.billingAddress.billingAddressStreet1 = studentPaymentDetails.studentBillingDetails.studentBillingStreet1;
  	this.billingAddress.billingAddressStreet2 = studentPaymentDetails.studentBillingDetails.studentBillingStreet2;
  	this.billingAddress.billingAddressCity = studentPaymentDetails.studentBillingDetails.studentBillingCity;
  	this.billingAddress.billingAddressState = studentPaymentDetails.studentBillingDetails.studentBillingState;
  	this.billingAddress.billingAddressCountry = studentPaymentDetails.studentBillingDetails.studentBillingCountry;
  	this.billingAddress.billingAddressPostalcode = studentPaymentDetails.studentBillingDetails.studentBillingPostalcode;
  }

  setBillingAsShipping(checked:boolean){
  	console.log("same as shipping")

  	if(checked) {
      this.billingAddress.billingAddressName = this.deliveryAddress.deliveryAddressName;
      this.billingAddress.billingAddressStreet1 = this.deliveryAddress.deliveryAddressStreet1;
      this.billingAddress.billingAddressStreet2 = this.deliveryAddress.deliveryAddressStreet2;
      this.billingAddress.billingAddressCity = this.deliveryAddress.deliveryAddressCity;
      this.billingAddress.billingAddressState = this.deliveryAddress.deliveryAddressState;
      this.billingAddress.billingAddressCountry = this.deliveryAddress.deliveryAddressCountry;
      this.billingAddress.billingAddressPostalcode = this.deliveryAddress.deliveryAddressPostalcode;
    } else {
      this.billingAddress.billingAddressName = "";
      this.billingAddress.billingAddressStreet1 = "";
      this.billingAddress.billingAddressStreet2 = "";
      this.billingAddress.billingAddressCity = "";
      this.billingAddress.billingAddressState = "";
      this.billingAddress.billingAddressCountry = "";
      this.billingAddress.billingAddressPostalcode = "";
    }
  }

  onSubmit(){
    console.log("deliveryMethod: "+ this.shippingMethod);
    this.setDeliveryAddress(this.studentDeliveryDetails);
  	this.studentCheckoutService.checkout(
      this.deliveryAddress,
      this.billingAddress,
      this.payment,
      this.shippingMethod
      ).subscribe(
      res=>{
        this.order=res.json();
        console.log(this.order);

        let navigationExtras: NavigationExtras = {
            queryParams: {
                "order": JSON.stringify(this.order)
            }
        };
        this.router.navigate(['/purchase-summary'], navigationExtras);
      },
      error=>{
        console.log(error.text());
      }
      );
    }

    ngOnInit() {
      this.getSelectedBookList();
      this.getSelectedNotesList();
      this.getSelectedLabList();

      this.studentCartService.getStudentCart().subscribe(
        res=>{
          console.log(res.json());
          this.studentCart=res.json();
        },
        error=>{
          console.log(error.text());
        }
        );

        for (let s in AppProperties.usStates) {
               this.stateList.push(s);
        }
      }
  }
