import { Component, OnInit } from '@angular/core';
import { LabMaterial } from '../../../models/lab';
import { GetLabListService } from '../../../services/get-lab-list.service';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { Http } from '@angular/http';
import { AppProperties } from '../../../constants/app-properties';

@Component({
	selector: 'app-shop-lab-list',
	templateUrl: './shop-lab-list.component.html',
	styleUrls: ['./shop-lab-list.component.scss']
})
export class ShopLabListComponent implements OnInit {
	public filterQuery = "";
	public rowsOnPage = 5;
	private selectedLab: LabMaterial;
	private labList: LabMaterial[];
	private serverPath = AppProperties.serverPath;

	constructor(
		private getLabListService: GetLabListService,
		private router: Router,
		private http: Http,
		private route: ActivatedRoute
	) { }

	onSelect(lab: LabMaterial) {
		this.selectedLab = lab;
		this.router.navigate(['/lab-detail', this.selectedLab.id]);
	}

	ngOnInit() {
		this.route.queryParams.subscribe(params => {
			if (params['labList']) {
				console.log("shopping lab list");
				this.labList = JSON.parse(params['labList']);
			} else {
				this.getLabListService.getLabList().subscribe(
					res => {
						console.log(res.json());
						this.labList = res.json();
					},
					err => {
						console.log(err);
					}
				);
			}
		});
	}
}


