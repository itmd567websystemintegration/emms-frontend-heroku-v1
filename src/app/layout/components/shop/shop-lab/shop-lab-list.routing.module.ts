import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShopLabListComponent } from './shop-lab-list.component';

const routes: Routes = [
    { path: '', component: ShopLabListComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShopLabListRoutingModule { }
