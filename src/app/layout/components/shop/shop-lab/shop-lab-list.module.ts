import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ShopLabListRoutingModule } from './shop-lab-list.routing.module';
import { ShopLabListComponent } from './shop-lab-list.component';
import { PageHeaderModule } from './../../../../shared';
import { GetLabListService } from './../../../services/get-lab-list.service';
import { ShopLabDataFilterPipe } from './../../../components/shop/shop-lab/data-filter.pipe';
import { DataTableModule } from 'angular2-datatable';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ShopLabListRoutingModule,
        PageHeaderModule,
        DataTableModule
    ],
declarations: [ShopLabListComponent, ShopLabDataFilterPipe],
providers: [
    GetLabListService,
  ]
})
export class ShopLabListModule { }