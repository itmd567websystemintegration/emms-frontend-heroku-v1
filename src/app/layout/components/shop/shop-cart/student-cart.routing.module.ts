import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StudentCartComponent } from './student-cart.component';

const routes: Routes = [
    { path: '', component: StudentCartComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StudentCartRoutingModule { }
