import { Component, OnInit } from '@angular/core';
import { Notes } from '../../../models/notes';
import {GetNotesListService} from '../../../services/get-notes-list.service';
import {Params, ActivatedRoute, Router} from '@angular/router';
import {Http} from '@angular/http';
import {AppProperties} from '../../../constants/app-properties';

@Component({
	selector: 'app-shop-notes-list',
	templateUrl: './shop-notes-list.component.html',
	styleUrls: ['./shop-notes-list.component.scss']
})
export class ShopNotesListComponent implements OnInit {
	public filterQuery = "";
	public rowsOnPage = 5;
	private selectedNotes: Notes;
	private notesList: Notes[];
	private serverPath = AppProperties.serverPath;

	constructor(
		private getNotesListService: GetNotesListService,
		private router:Router,
		private http:Http,
		private route:ActivatedRoute
		) { }

	onSelect(notes: Notes) {
		this.selectedNotes = notes;
		this.router.navigate(['/notes-detail', this.selectedNotes.id]);
	}

	ngOnInit() {
		this.route.queryParams.subscribe(params => {
			if(params['notesList']) {
				console.log("shopping notes list");
				this.notesList = JSON.parse(params['notesList']);
			} else {
				this.getNotesListService.getNotesList().subscribe(
					res => {
						console.log(res.json());
						this.notesList = res.json();
					},
					err => {
						console.log(err);
					}
					);
			}
		});

	}
}


