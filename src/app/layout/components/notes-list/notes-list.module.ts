import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NotesListRoutingModule } from './notes-list.routing.module';
import { NotesListComponent } from './notes-list.component';
import { PageHeaderModule } from './../../../shared';
import { DataFilterPipe } from './../../components/notes-list/data-filter.pipe';
import { DataTableModule } from 'angular2-datatable';


import { GetNotesListService } from './../../services/get-notes-list.service';
import {DeleteNotesService} from '../../services/delete-notes.service';
import { AddItemService } from './../../services/add-item.service';
import { UploadImageService } from './../../services/upload-image.service';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NotesListRoutingModule,
        PageHeaderModule,
        DataTableModule
    ],
declarations: [NotesListComponent, DataFilterPipe],
providers: [
    AddItemService,
    UploadImageService,
    GetNotesListService,
    DeleteNotesService
  ]
})
export class NotesListModule { }
