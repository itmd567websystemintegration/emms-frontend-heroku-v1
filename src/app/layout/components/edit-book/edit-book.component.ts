import { Component, OnInit } from '@angular/core';
// import { MatGridListModule } from '@angular/material';
import { UploadImageService } from '../../services/upload-image.service';
import { Book } from '../../models/book';
import { Course } from '../../models/course';
import { Department } from '../../models/department';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { GetBookService } from '../../services/get-book.service';
import { GetDepartmentListService } from '../../services/get-department-list.service';
import { EditBookService } from '../../services/edit-book.service';
import { GetCourseListService } from '../../services/get-course-list.service';

@Component({
	selector: 'app-edit-book',
	templateUrl: './edit-book.component.html',
	styleUrls: ['./edit-book.component.scss']
})
export class EditBookComponent implements OnInit {
	private bookId: number;
	private book: Book = new Book();
	private department: Department = new Department();
	private course: Course = new Course();
	private bookUpdated: boolean;
	private departmentList: Department[] = [];
	private courseList: Course[] = [];

	constructor(
		private uploadImageService: UploadImageService,
		private editBookService: EditBookService,
		private getBookService: GetBookService,
		private getCourseListService: GetCourseListService,
		private getDepartmentListService: GetDepartmentListService,
		private route: ActivatedRoute,
		private router: Router
	) { }

	onSubmit() {
		this.editBookService.editBook(this.book).subscribe(

			data => {
				console.log("edited book: " + this.book);
				this.uploadImageService.modify(JSON.parse(JSON.parse(JSON.stringify(data))._body).id);
				this.bookUpdated = true;
				this.book = new Book();
			},
			error => console.log(error)
		);
	}

	ngOnInit() {
		this.route.params.forEach((params: Params) => {
			this.bookId = Number.parseInt(params['id']);
		});

		this.getBookService.getBook(this.bookId).subscribe(
			res => {
				this.book = res.json();
				this.department = this.book.department;
				this.course = this.book.course;
			},
			error => console.log(error)
		)

		this.getDepartmentListService.getDepartmentList().subscribe(
			res => {
				console.log(res.json());
				this.departmentList = res.json();
			},
			error => {
				console.log(error.text());
			}
		);

		this.getCourseListService.getCourseList().subscribe(
			res => {
				console.log(res.json());
				this.courseList = res.json();
			},
			error => {
				console.log(error.text());
			}
		);
	}

}
