import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BookListRoutingModule } from './book-list.routing.module';
import { BookListComponent } from './book-list.component';
import { PageHeaderModule } from './../../../shared';
import { DataFilterPipe } from './../../components/book-list/data-filter.pipe';
import { DataTableModule } from 'angular2-datatable';


import { GetBookListService } from './../../services/get-book-list.service';
import {DeleteBookService} from '../../services/delete-book.service';
import { AddItemService } from './../../services/add-item.service';
import { UploadImageService } from './../../services/upload-image.service';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        BookListRoutingModule,
        PageHeaderModule,
        DataTableModule
    ],
declarations: [BookListComponent, DataFilterPipe],
providers: [
    AddItemService,
    UploadImageService,
    GetBookListService,
    DeleteBookService
  ]
})
export class BookListModule { }
