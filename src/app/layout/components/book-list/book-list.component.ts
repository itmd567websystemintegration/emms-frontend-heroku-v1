import { Component, OnInit } from '@angular/core';
import { Book } from '../../models/book';
import { Router } from '@angular/router';
// import {LoginService} from '../../services/login.service';
import { GetBookListService } from '../../services/get-book-list.service';
import { DeleteBookService } from '../../services/delete-book.service';


@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss']
})
export class BookListComponent implements OnInit {
  private selectedBook: Book;
  private checked: boolean;
  private bookList: Book[];
  private allChecked: boolean;
  private deleteBookList: Book[] = new Array();
  public rowsOnPage = 5;
  constructor(
    private getBookListService: GetBookListService,
    private deleteBookService: DeleteBookService,
    private router: Router
  ) { }

  onSelect(book: Book) {
    this.selectedBook = book;
    console.log("book-list: " + book);
    this.router.navigate(['/view-book', this.selectedBook.id]);
  }

  onDelete(book: Book) {
    console.log('delete called...');
    this.deleteBookService.deleteBook(book.id).subscribe(
      res => {
        console.log(res);
        this.getBookListByLoggedInStudent();
      },
      err => {
        console.log(err);
      }
    );
  }


  updateDeleteBookList(checked: boolean, book: Book) {
    if (checked) {
      this.deleteBookList.push(book);
    } else {
      this.deleteBookList.splice(this.deleteBookList.indexOf(book), 1);
    }
  }

  updateSelected(checked: boolean) {
    if (checked) {
      this.allChecked = true;
      this.deleteBookList = this.bookList.slice();
    } else {
      this.allChecked = false;
      this.deleteBookList = [];
    }
  }

  removeSelectedBooks() {
    for (let book of this.deleteBookList) {
      this.deleteBookService.deleteBook(book.id).subscribe(
        res => {
          this.getBookListByLoggedInStudent();
        },
        err => {
        }
      );
    }
    location.reload();
  }


  getBookListByLoggedInStudent() {
    this.getBookListService.getBookListByLoggedInStudent().subscribe(
      res => {        
        this.bookList = res.json();
      },
      error => {
        console.log(error);
      }
    );
  }

  ngOnInit() {
    console.log('on init called');
    this.getBookListByLoggedInStudent();
  }
}