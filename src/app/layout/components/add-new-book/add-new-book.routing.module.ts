import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddNewBookComponent } from './add-new-book.component';

const routes: Routes = [
    { path: '', component: AddNewBookComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddNewBookRoutingModule { }
