import { Component, OnInit } from '@angular/core';
import { StudentOrder } from '../../models/student-order';
import { Router } from '@angular/router';
import { StudentOrderService } from '../../services/order.service';


@Component({
  selector: 'app-order-history-list',
  templateUrl: './order-history.component.html',
  styleUrls: ['./order-history.component.scss']
})
export class OrderHistoryComponent implements OnInit {
  private studentOrderList: StudentOrder[];
  private studentOrder: StudentOrder;
  public rowsOnPage = 5;
  constructor(
    private studentOrderService: StudentOrderService,
    private router: Router
  ) { }

  onSelect(studentOrder: StudentOrder) {
    this.studentOrder = studentOrder;
    console.log("order-history: " + JSON.stringify(this.studentOrder));
    this.router.navigate(['/view-order', this.studentOrder.id]);
  }

  getOrderHistoryByLoggedInStudent() {
    this.studentOrderService.getStudentOrderList().subscribe(
      res => {        
        this.studentOrderList = res.json();
      },
      error => {
        console.log(error);
      }
    );
  }

  ngOnInit() {
    console.log('on init called');
    this.getOrderHistoryByLoggedInStudent();
  }
}