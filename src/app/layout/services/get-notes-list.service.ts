import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';

@Injectable()
export class GetNotesListService {
  private idToken: string;

  constructor(private http:Http) { }

  getNotesList() {
  	let url = "http://localhost:8181/item/notesList";
    let headers = new Headers ({
      'Content-Type': 'application/json'
    });
    return this.http.get(url, {headers: headers});
  }

  getNotesListByLoggedInStudent() {
    this.idToken = localStorage.getItem('id_token');
    let studentDetails = {
      "id_token": this.idToken
    }

  	let url = "http://localhost:8181/item/notesListByLoggedInStudent";
    let tokenHeader = new Headers ({
      'Content-Type': 'application/json'
    });
    return this.http.post(url, JSON.stringify(studentDetails), { headers: tokenHeader });
  }
}
