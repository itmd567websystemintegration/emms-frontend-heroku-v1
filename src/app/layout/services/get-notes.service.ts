import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';


@Injectable()
export class GetNotesService {

  constructor(private http:Http) { }

  getNotes(id:number) {
  	let url = "http://localhost:8181/item/notes/"+id;
    let headers = new Headers ({
      'Content-Type': 'application/json'
    });

    return this.http.get(url, {headers: headers});
  }
}
