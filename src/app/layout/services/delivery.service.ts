import { Injectable } from '@angular/core';
import { AppProperties } from '../constants/app-properties';
import { Http, Headers } from '@angular/http';
import { StudentDeliveryDetails } from '../models/student-delivery-details';

@Injectable()
export class StudentDeliveryDetailsService {

  private serverPath: string = AppProperties.serverPath;

  constructor(private http : Http) { }

  addDelivery(delivery: StudentDeliveryDetails) {
  	let url = this.serverPath+"/delivery/add";

  	let tokenHeader = new Headers({
  		'Content-Type' : 'application/json'
  		// 'x-auth-token' : localStorage.getItem("xAuthToken")
  	});
  	return this.http.post(url, JSON.stringify(delivery), {headers: tokenHeader});
  }

  getStudentDeliveryDetailsList() {
  	let url = this.serverPath+"/delivery/getStudentDeliveryDetailsList";

  	let tokenHeader = new Headers({
  		'Content-Type' : 'application/json'
  		// 'x-auth-token' : localStorage.getItem("xAuthToken")
  	});
  	return this.http.get(url, {headers: tokenHeader});
  }

  deleteDelivery(id:number){
  	let url = this.serverPath+"/delivery/delete";

  	let tokenHeader = new Headers({
  		'Content-Type' : 'application/json'
  		// 'x-auth-token' : localStorage.getItem("xAuthToken")
  	});
  	return this.http.post(url, id, {headers: tokenHeader});
  }

  setDefaultDelivery(id:number){
  	let url = this.serverPath+"/delivery/setDefault";

  	let tokenHeader = new Headers({
  		'Content-Type' : 'application/json'
  		// 'x-auth-token' : localStorage.getItem("xAuthToken")
  	});
  	return this.http.post(url, id, {headers: tokenHeader});
  }
}
