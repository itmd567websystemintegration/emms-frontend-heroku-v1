import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {Book} from '../models/book';

@Injectable()
export class EditBookService {

  constructor(private http:Http) { }

  editBook(book:Book) {
  	let url = "http://localhost:8181/item/update";
    
    let headers = new Headers ({
      'Content-Type': 'application/json'
    });

    return this.http.post(url, JSON.stringify(book), {headers: headers});
  }
}
