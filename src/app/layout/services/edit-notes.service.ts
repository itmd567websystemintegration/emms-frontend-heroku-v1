import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {Notes} from '../models/notes';

@Injectable()
export class EditNotesService {

  constructor(private http:Http) { }

  editNotes(notes:Notes) {
  	let url = "http://localhost:8181/item/notes/update";
    
    let headers = new Headers ({
      'Content-Type': 'application/json'
    });

    return this.http.post(url, JSON.stringify(notes), {headers: headers});
  }
}
