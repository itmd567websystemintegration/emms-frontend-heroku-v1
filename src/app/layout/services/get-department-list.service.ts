import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import { Department } from '../models/department';
@Injectable()
export class GetDepartmentListService {

  constructor(private http:Http) { }

  getDepartmentList() {
  	let url = "http://localhost:8181/department/departmentList";
    let headers = new Headers ({
      'Content-Type': 'application/json'
    });

    return this.http.get(url, {headers: headers});
  }

  addDepartment(department:Department) {
    console.log("addDepartment(): "+department);
  	let url = "http://localhost:8181/department/addDepartment";
    
    let headers = new Headers ({
      'Content-Type': 'application/json'
    });

    return this.http.post(url, JSON.stringify(department), {headers: headers});
  }

}
