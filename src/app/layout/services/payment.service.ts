import { Injectable } from '@angular/core';
import { AppProperties } from '../constants/app-properties';
import { Http, Headers } from '@angular/http';
import { StudentPaymentDetails } from '../models/student-payment-details';

@Injectable()
export class PaymentService {
  private serverPath: string = AppProperties.serverPath;

  constructor(private http : Http) { }

  addPayment(payment: StudentPaymentDetails) {
  	let url = this.serverPath+"/payment/add";

  	let tokenHeader = new Headers({
  		'Content-Type' : 'application/json'
  		// 'x-auth-token' : localStorage.getItem("xAuthToken")
  	});
  	return this.http.post(url, JSON.stringify(payment), {headers: tokenHeader});
  }

  getStudentPaymentList() {
  	let url = this.serverPath+"/payment/getStudentPaymentList";

  	let tokenHeader = new Headers({
  		'Content-Type' : 'application/json'
  		// 'x-auth-token' : localStorage.getItem("xAuthToken")
  	});
  	return this.http.get(url,  {headers: tokenHeader});
  }

  deletePayment(id: number) {
  	let url = this.serverPath+"/payment/delete";

  	let tokenHeader = new Headers({
  		'Content-Type' : 'application/json'
  	});
  	return this.http.post(url, id, {headers: tokenHeader});
  }

  setDefaultPayment (id: number) {
  	let url = this.serverPath+"/payment/setDefault";

  	let tokenHeader = new Headers({
  		'Content-Type' : 'application/json'
  	});
  	return this.http.post(url, id, {headers: tokenHeader});
  }
}
