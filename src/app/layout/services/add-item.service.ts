import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {Book} from '../models/book';
import { Notes } from '../models/notes';
import { LabMaterial } from '../models/lab';

@Injectable()
export class AddItemService {

  constructor(private http:Http) { }

  addBook(book:Book) {
    console.log("AddBookService:addBook(): "+book);
  	let url = "http://localhost:8181/item/addBook";
    
    let headers = new Headers ({
      'Content-Type': 'application/json'
    });

    return this.http.post(url, JSON.stringify(book), {headers: headers});
  }

  addNotes(notes:Notes) {
    console.log("AddNotesService:addNotes(): "+notes);
  	let url = "http://localhost:8181/item/addNotes";
    
    let headers = new Headers ({
      'Content-Type': 'application/json'
    });

    return this.http.post(url, JSON.stringify(notes), {headers: headers});
  }

  addLab(labMaterial:LabMaterial) {
    console.log("AddLabsService:addLabMaterial(): "+labMaterial);
  	let url = "http://localhost:8181/item/addLabs";
    
    let headers = new Headers ({
      'Content-Type': 'application/json'
    });

    return this.http.post(url, JSON.stringify(labMaterial), {headers: headers});
  }

}
