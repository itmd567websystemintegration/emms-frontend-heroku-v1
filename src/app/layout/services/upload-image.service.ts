import { Injectable } from '@angular/core';

@Injectable()
export class UploadImageService {

  filesToUpload: Array<File>;

  constructor() {
  	this.filesToUpload = [];
  }

  upload(itemId: number, itemType: string) {
		console.log('itemId: '+itemId);
  	this.makeFileRequest("http://localhost:8181/item/add/image?id="+itemId+"&itemType="+itemType, [], this.filesToUpload).then((result) => {
  		console.log(result);
  	}, (error) => {
  		console.log(error);
  	});
  }

  modify(itemId: number) {
    console.log(this.filesToUpload);
    if (this.filesToUpload.length>0) {
      this.makeFileRequest("http://localhost:8181/item/update/image?id="+itemId, [], this.filesToUpload).then((result) => {
      console.log(result);
    }, (error) => {
      console.log(error);
    });
    }
  }

  fileChangeEvent(fileInput: any) {
		this.filesToUpload = <Array<File>> fileInput.target.files;
		console.log('file ready for upload');
  }

  makeFileRequest(url:string, params:Array<string>, files:Array<File>) {
  	return new Promise((resolve, reject) => {
  		var formData:any = new FormData();
  		var xhr = new XMLHttpRequest();
  		for(var i=0; i<files.length;i++) {
				console.log('files[i]: '+files[i]+' '+files[i].name);
  			formData.append("uploads[]", files[i], files[i].name);
  		}
  		xhr.onreadystatechange = function() {
  			if(xhr.readyState == 4) {
  				if(xhr.status==200) {
  					console.log("image uploaded successfully!");
  				} else {
  					reject(xhr.response);
  				}
  			}
  		}

  		xhr.open("POST", url, true);
  		xhr.setRequestHeader("x-auth-token", localStorage.getItem("xAuthToken"));
  		xhr.send(formData);
  	});
  }

}
