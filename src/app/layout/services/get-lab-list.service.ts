import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';

@Injectable()
export class GetLabListService {
  private idToken: string;

  constructor(private http:Http) { }

  getLabList() {
  	let url = "http://localhost:8181/item/labsList";
    let headers = new Headers ({
      'Content-Type': 'application/json'
    });
    return this.http.get(url, {headers: headers});
  }

  getLabListByLoggedInStudent() {
    this.idToken = localStorage.getItem('id_token');
    let studentDetails = {
      "id_token": this.idToken
    }

  	let url = "http://localhost:8181/item/labListByLoggedInStudent";
    let tokenHeader = new Headers ({
      'Content-Type': 'application/json'
    });
    return this.http.post(url, JSON.stringify(studentDetails), { headers: tokenHeader });
  }
}
