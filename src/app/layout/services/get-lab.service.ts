import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';


@Injectable()
export class GetLabService {

  constructor(private http:Http) { }

  getLab(id:number) {
  	let url = "http://localhost:8181/item/labs/"+id;
    let headers = new Headers ({
      'Content-Type': 'application/json'
    });

    return this.http.get(url, {headers: headers});
  }
}
