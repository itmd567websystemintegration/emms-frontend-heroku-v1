import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddBookRoutingModule } from './add-book-routing.module';
import { AddBookComponent } from './add-book.component';
import { PageHeaderModule } from './../../../../shared';

@NgModule({
    imports: [
        CommonModule,
        AddBookRoutingModule,
        PageHeaderModule
    ],
declarations: [AddBookComponent]
})
export class AddBookModule { }
