import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../../router.animations';

@Component({
    selector: 'add-lab',
    templateUrl: './add-lab.component.html',
    styleUrls: ['./add-lab.component.scss'],
    animations: [routerTransition()]
})
export class AddLabComponent implements OnInit {
    constructor() { }
    ngOnInit() {}
}
